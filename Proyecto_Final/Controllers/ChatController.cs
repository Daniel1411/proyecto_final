﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class ChatController :BaseController
    {
        // GET: Chat
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult ListadoChat()
        {
            return View();
        }
        private List<Object> ObtenerListaChats(int idUsuario)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = string.Format("SELECT * FROM {0} WHERE idemisor={1}", _table, idUsuario);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> resultado = new List<Object>();
                while (rdr.Read())
                {
                    resultado.Add(ToModel(rdr));
                }
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }
        public ActionResult Chat()
        {
            return View();
        }
        // GET: Chat/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

      

        // POST: Chat/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                string msg = collection["mensaje"];

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Chat/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Chat/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Chat/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Chat/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


         public ChatController() : base()
        {
           this._model = new Chat();
           this._table = "chat";
        }


       
          public override Object ToModel(MySqlDataReader rdr)
        {
            Chat chat = new Chat();
            chat.IdChat = Convert.ToInt32(rdr["idChat"]);
            chat.IdUsuarioEmisor = Convert.ToInt32(rdr["idusuarioemisor"]);
            chat.IdUsuarioReceptor = Convert.ToInt32(rdr["idusuarioreceptor"]);

            return chat; 
        }
    }
}
