﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using Proyecto_Final.Controllers;

namespace Proyecto_Final.Controllers
{
    public abstract class BaseController: Controller
    {
        public static MySqlConnection _conn;
        public Object _model;
        public string _table;

        static BaseController()
        {
            string connStr = "server=127.0.0.1;user=root;database=gallinas;port=3306;password=admin";
            _conn = new MySqlConnection(connStr);
        }

        public abstract Object ToModel(MySqlDataReader rdr);

        public List<Object> getAll(string query=null)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = (query==null) ? string.Format("SELECT * FROM {0} limit 100", _table) : query;
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> resultado = new List<Object>();
                while (rdr.Read())
                {
                    resultado.Add(ToModel(rdr));
                }
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public Object getId(string colId, int id, string query = null)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = (query == null) ? string.Format("SELECT * FROM {0} WHERE {1}={2}", _table, colId, id) : query;
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Read();
                Object ob = ToModel(rdr);
                rdr.Close();
                _conn.Close();
                return ob;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public string getValue(string column, int id)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = string.Format("SELECT {0} FROM {1} WHERE id={2}", column, _table, id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Read();
                string resultado = rdr[0].ToString();
                rdr.Close();
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public MySqlDataReader getIdFromTable(string column, int id, string table)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = string.Format("SELECT * FROM {0} WHERE {1}={2}", table, column, id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
                return rdr;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public void Update(int id, string column, int value)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = string.Format("UPDATE {0} SET {1}={2} WHERE id={3}", _table, column, value, id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        public void Update(int id, string titulo, string contenido)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                MySqlCommand cmd = new MySqlCommand("UPDATE articles SET titol = @titol, contingut = @contingut WHERE id= @id", _conn);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@titol", titulo);
                cmd.Parameters.AddWithValue("@contingut", contenido);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();

                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        public void Delete(int id)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                MySqlCommand cmd = new MySqlCommand("DELETE FROM articles WHERE id= @id", _conn);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();

                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

       /*public void Create(string titulo, string fecha, string contenido)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                MySqlCommand cmd = new MySqlCommand("INSERT INTO articles(titol, `data`, contingut, `like`, unlike) VALUES(@titol, @data, @contingut, @like, @unlike)", _conn);

                cmd.Prepare();
                cmd.Parameters.AddWithValue("@titol", titulo);
                cmd.Parameters.AddWithValue("@data", fecha);
                cmd.Parameters.AddWithValue("@contingut", contenido);
                cmd.Parameters.AddWithValue("@like", 0);
                cmd.Parameters.AddWithValue("@unlike", 0);
                cmd.ExecuteNonQuery();

                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }*/

        public List<T> ConvertList<T>(List<object> value) where T : class
        {
            List<T> newlist = value.Cast<T>().ToList();
            return newlist;
        }


        public void closeConn()
        {
            if (_conn.State == System.Data.ConnectionState.Open)
            {
                _conn.Close();
            }

        }
        public void openConn()
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }

        }
    }
}