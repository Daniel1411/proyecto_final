﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class ProvinciasController : BaseController
    {
        // GET: Provincias
        public ActionResult Index()
        {
            return View();
        }
         public ProvinciasController() : base()
        {
           this._model = new Provincias();
           this._table = "provincias";
        }


        public override Object ToModel(MySqlDataReader rdr)
        {
            Provincias Provincias = new Provincias();
            Provincias.IdProvincias = Convert.ToInt32(rdr["idProvincias"]);
            Provincias.Nombre = rdr["provincia"].ToString();
            return Provincias; 
        }
        public List<Provincias> GetProvincias()
        {
            MySqlConnection _conn = null;
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            List<Provincias> lista = new List<Provincias>();//creamos una lista para recoger tofdos los datos

            try//bloque try para controlar las excepciones.
            {
                _conn = new MySqlConnection(connStr);//utilizamos el constructor para crear el objeto de conexion

                string tabla = "provincias";
                _conn.Open();//abrimos la sesion con el servidor SQl
                string sql = string.Format("SELECT * FROM {0} ", tabla);//metemos la consulta SQL en un string 
                MySqlCommand cmd = new MySqlCommand(sql, _conn);//le pasamos la consulta y el obejo que habre la conexion
                MySqlDataReader rdr = cmd.ExecuteReader();//este objeto nos devuelve los datos que hemos consultado 

                while (rdr.Read())//se situa en la primera posicion si hay datos entra en el if sino hay datos devovera falso
                {
                    Provincias al = new Provincias();//rellenamos el objeto con los datos que nos devuelve la consulta 
                    al.IdProvincias = (int)rdr["idProvincias"];
                    al.Nombre = rdr["Provincia"].ToString();//dentro de los corchetes escribimos exactamente el nombre de los campos de la base de datos


                    lista.Add(al);//cada vez que da una vuelta al bucle añade un objeto alumno

                }
                rdr.Close();//cerramos el evento rdr

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally//siempre llegamos aqui ocurra lo que ocurra
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }
            }
            return lista;//devuelve un objeto con los datos de la base de datos

        }
    }

}