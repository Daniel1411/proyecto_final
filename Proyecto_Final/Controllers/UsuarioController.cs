﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class UsuarioController : BaseController
    {
        ComentarioVentaController comentarioVentaController;
        GallinaController gallinaController;

        public UsuarioController() : base()
        {
           this._model = new Usuario();
           this._table = "usuario";
           this.comentarioVentaController = new ComentarioVentaController();
           this.gallinaController = new GallinaController();
        }

        // GET: Usuario
        public ActionResult Index()
        {
            // convertim llista d'objectes en llista d'usuaris
            List<Usuario> Model = this.getAll().Cast<Usuario>().ToList();
            return View(Model);
        }

        private void ActualizarSesion()
        {
            if (String.IsNullOrEmpty(Session["controller"] as String))
            {
                this.HttpContext.Session.Add("controller", "Home");
            }
            else
            {
                this.HttpContext.Session["controller"] = "Home";
            }
            if (String.IsNullOrEmpty(Session["view"] as String))
            {
                this.HttpContext.Session.Add("view", "Index");
            }
            else
            {
                this.HttpContext.Session["view"] = "Index";
            }
        }

        public ActionResult Perfil(int idUsuario)
        {

            int loggedid = 0;
            if (!String.IsNullOrEmpty(Session["logged"] as String))
            {
                loggedid = Convert.ToInt32(Session["loggedid"]);
            }
            Usuario usuario = this.ObtenerDatosUsuario(idUsuario, loggedid);
            
            return View(usuario);
        }

        private Usuario ObtenerDatosUsuario(int idUsuario, int loggedid)
        {
            string sql = "SELECT u.*, p.Provincia";
            sql += " FROM usuario u";
            sql += " left join provincias p on p.idProvincias = u.idProvincias";
            sql += " where u.idUsuario='" + idUsuario + "';"; 
            Usuario usuario = (Usuario) this.getId("idUsuario", idUsuario, sql);
            usuario.Gallinas = this.ObtenerListadoGallinas(loggedid, idUsuario);
            usuario.Comentarios = this.ObtenerListadoComentarios(idUsuario);
            usuario.Puntuacion = this.ObtenerPuntuacion();
            usuario.ComentariosPendientesVentas = this.ComprobarVentas(loggedid);
            return usuario;
        }

        private List<ComentarioVentaPendiente> ComprobarVentas(int loggedid)
        {
            VentasController ventasController = new VentasController();
            return ventasController.ComprobarVentasUsuario(loggedid);
        }


        public ActionResult Logout(string actionName, string controllerName, int idUsuarioPerfil, int idGallinaDetail, int idBlog)
        {
            this.HttpContext.Session.Remove("loggedid");
            this.HttpContext.Session.Remove("logged");
            this.HttpContext.Session.Remove("loggedfoto");
            if (actionName == "Index" && controllerName == "Home")
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actionName == "Perfil")
            {
                return RedirectToAction("Perfil", "Usuario", new { idUsuario = idUsuarioPerfil });
            }
            else if (actionName == "Detail")
            {
                return RedirectToAction("Detail", "Gallina", new { id = idGallinaDetail });
            }
            else if (actionName == "Blog" && controllerName == "Blog")
            {
                return RedirectToAction("Blog", "Blog", new { idBlog });
            }
            else if (controllerName == "Blog")
            {
                return RedirectToAction("Index", "Blog");
            }
            return RedirectToAction("Index", "Home");
        }


        private List<Gallina> ObtenerListadoGallinas(int loggedid, int idUsuario)
        {
            return this.gallinaController.ObtenerGallinas(loggedid, idUsuario);
        }

        private double ObtenerPuntuacion()
        {
            return this.comentarioVentaController.Puntuacion;
        }

        private List<ComentarioVenta> ObtenerListadoComentarios(int idUsuario)
        {
            List<Object> comentariosObj = this.comentarioVentaController.ObtenerComentarios(idUsuario);
            return this.comentarioVentaController.ConvertList<ComentarioVenta>(comentariosObj);
        }

        public ActionResult Favoritos(int idUsuario)
        {
            List<Gallina> gallinas = this.ObtenerListadoGallinasFavoritas(idUsuario);
            return View(gallinas);
        }

        private List<Gallina> ObtenerListadoGallinasFavoritas(int idUsuario)
        {
            return this.ConvertList<Gallina>(this.gallinaController.ObtenerGallinasFavoritas(idUsuario));
        }
        public void SelectLocalidad()
        {
            ProvinciaController craza = new ProvinciaController();
            List<Provincia> ll = craza.GetProvincias();
            List<SelectListItem> sl = new List<SelectListItem>();
            foreach (Provincia r in ll)
            {
                SelectListItem item = new SelectListItem();
                item.Value = r.IdProvincias.ToString();
                item.Text = r.Nombre;
                sl.Add(item);
            }

            ViewBag.Localidad = sl;
        }

        //GET
        public ActionResult Registro()
        {
            this.SelectLocalidad();
            return View();
        }
        public string GuardarFoto(HttpPostedFileBase file, string profileFoto)
        {
            Random rmd = new Random();

            int x = rmd.Next(0, 3000);

            string db_path = "null";

            if (file != null && file.FileName.Length > 0)//si no hay audio FileName esta vacio
            {
                //ojo con posibles espacios en blanco ...
                string pic = "Foto" + x.ToString() + profileFoto + System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/PerfilesUsuarios"), pic);
                db_path = "/Images/PerfilesUsuarios/" + pic;
                // file is uploaded
                file.SaveAs(path);
            }
            return db_path;
        }//OK
        // POST: Registro
        [HttpPost]
        public ActionResult Registro(FormCollection collection)
        {

            // TODO: Add insert logic here
            openConn(); //método propio que abre conexión si está cerrada
                        //obtenemos email y pass, codificamos pass
            HttpPostedFileBase file = Request.Files[0];
           
            string Nombre = collection["Nombre"] as String;
            string Apellidos = collection["Apellidos"] as String;
            string Edad = collection["Edad"] as String;
            string IdProvincia = collection["IdProvincias"] as String;
            string Email =  collection["Email"] as String;
            string Password = collection["Password"] as String;
            string ConfirmPassword = collection["ConfirmPassword"] as String;
            string Foto = this.GuardarFoto(file,Nombre);
            if (emailExisteix(Email))
            {
                ViewBag.ErrorMensaje = "Ya existe este correo";
            }
            Password = Codifica.ConverteixPassword(Password);
            
            try
            {
                string sql = "INSERT INTO usuario (nombre, apellidos, edad, idProvincias,  correo, password, foto) VALUES (@Nombre, @Apellidos, @Edad, @IdProvincia, @Email, @Password,@Foto);";
                MySqlCommand cmd = new MySqlCommand();
                openConn();
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Parameters.AddWithValue("@Nombre", Nombre);
                cmd.Parameters.AddWithValue("@Apellidos", Apellidos);
                cmd.Parameters.AddWithValue("@Edad", Edad);
                cmd.Parameters.AddWithValue("@idProvincia", IdProvincia);
                cmd.Parameters.AddWithValue("@Email", Email);
                cmd.Parameters.AddWithValue("@Password", Password);
                cmd.Parameters.AddWithValue("@Foto", Foto);
                cmd.Prepare();
                cmd.ExecuteNonQuery();

                //select per trobar darrer id
                sql = "SELECT LAST_INSERT_ID() AS ultim;";
                MySqlCommand cmd2 = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd2.ExecuteReader();
                if (rdr.Read())
                {
                    string cosa = rdr["ultim"].ToString();
                    int idinsertat = Convert.ToInt32(cosa);
                    //this.HttpContext.Session.Add("logged", nombre + idinsertat);
                    this.HttpContext.Session.Add("loggedid", idinsertat.ToString());
                    this.HttpContext.Session.Add("logged",Nombre);                   
                    this.HttpContext.Session.Add("loggedfoto", Foto);

                }



                closeConn(); //método propio que cierra conexión si está abierta

   

                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                string s = e.Message;
                ViewBag.MsgError = "S'ha produit un error...";
                return View();
            }
            
        }

        public override Object ToModel(MySqlDataReader rdr)
        {
            Usuario usuario = new Usuario();
            usuario.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);
            usuario.Nombre = rdr["nombre"].ToString();
            usuario.Apellidos = rdr["apellidos"].ToString();
            usuario.Edad = Convert.ToDateTime(rdr["edad"]);
            usuario.FechadeAlta = Convert.ToDateTime(rdr["fechaAlta"]);
            usuario.IdProvincias = Convert.ToInt32(rdr["idProvincias"]);
            usuario.Provincia = rdr["Provincia"].ToString();
            usuario.Email = rdr["correo"].ToString();
            usuario.Password = rdr["password"].ToString();
            usuario.Foto = rdr["foto"].ToString();
           
            return usuario; 
        }

        // POST: Login/Create
        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
           
                // TODO: Add insert logic here
                openConn(); //método propio que abre conexión si está cerrada
                            //obtenemos email y pass, codificamos pass
                string email = collection["email"].ToString();
                string pass = collection["password"].ToString();
                pass = Codifica.ConverteixPassword(pass);
                string controllerName = collection["controllerName"].ToString();
                string actionName = collection["actionName"].ToString();
                int idUsuarioPerfil = collection["idUsuarioPerfil"] != "" ? Convert.ToInt32(collection["idUsuarioPerfil"]) : -1;
                int idGallinaDetail = collection["idGallinaDetail"] != "" ? Convert.ToInt32(collection["idGallinaDetail"]) : -1;
                int idBlog = collection["idBlog"] != "" ? Convert.ToInt32(collection["idBlog"]) : -1;

                int loggeid = -1;
                try
                {
                    string sql = "SELECT * FROM usuario WHERE correo=@email and password=@password";
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandText = sql;
                    cmd.Connection = _conn;
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@password", pass);
                    cmd.Prepare();
                    MySqlDataReader rdr = cmd.ExecuteReader();
                    Usuario Model = null;
                    if (rdr.Read())
                    {
                        int _id = 0;
                        Int32.TryParse(rdr["idUsuario"].ToString(), out _id);
                        Model = new Usuario();
                        Model.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);
                        loggeid = Model.IdUsuario;
                        Model.Nombre = rdr["nombre"].ToString();
                        Model.Password = rdr["password"].ToString(); ;
                        Model.Email = rdr["correo"].ToString();
                        Model.Foto = rdr["foto"].ToString();
                    }

                    rdr.Close();
                    closeConn(); //método propio que cierra conexión si está abierta

                    if (Model != null)
                    {
                    this.HttpContext.Session.Add("logged", Model.Nombre);
                    this.HttpContext.Session.Add("loggedid", Model.IdUsuario.ToString());
                    this.HttpContext.Session.Add("loggedfoto", Model.Foto.ToString());

                }


            }
            catch (Exception e)
            {
                //return View();
            }
            if (actionName == "Index" && controllerName == "Home")
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actionName == "Perfil")
            {
                return RedirectToAction("Perfil", "Usuario", new { idUsuario = idUsuarioPerfil });
            }
            else if (actionName == "Detail")
            {
                return RedirectToAction("Detail", "Gallina", new { id = idGallinaDetail });
            }
            else if (actionName == "Blog" && controllerName == "Blog")
            {
                return RedirectToAction("Blog", "Blog", new { idBlog });
            }
            else if (controllerName == "Blog")
            {
                return RedirectToAction("Index", "Blog");
            }
            return RedirectToAction("Index", "Home");
        }
        private bool emailExisteix(string email)
        {
            bool existeix = false;
            openConn();
            try
            {
                string sql = "SELECT * FROM usuarios WHERE Email=@email";
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Prepare();
                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    existeix = true;
                }

                rdr.Close();
                closeConn(); //método propio que cierra conexión si está abierta

            }
            catch (Exception ex)
            {
                closeConn(); //método propio que cierra conexión si está abierta
            }

            return existeix;
        }

    }
}