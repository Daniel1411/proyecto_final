﻿using Proyecto_Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_Final.Controllers
{
    public class HomeController : Controller
    {
        private List<Gallina> ObtenerListadoGallinas(int loggedid)
        {
            GallinaController gallinaController = new GallinaController();
            return gallinaController.ObtenerGallinas(loggedid);
        }

        private List<Raza> ObtenerRazasGallinas()
        {
            RazaController razaController = new RazaController();
            List<Object> razasObj = razaController.getAll();
            return razaController.ConvertList<Raza>(razasObj);
        }

        private List<Provincia> ObtenerProvinciasGallinas()
        {
            ProvinciaController provinciaController = new ProvinciaController();
            List<Object> provinciasObj = provinciaController.getAll();
            return provinciaController.ConvertList<Provincia>(provinciasObj);
        }

        private List<Blog> ObtenerBlogsGallinas()
        {
            BlogController blogController = new BlogController();
            List<Object> blogObj = blogController.getAll();
            return blogController.ConvertList<Blog>(blogObj);
        }

        private Home ObtenerDatos(int loggedid,  string selectRaza, string selectProvincia, string inputPoblacion, string inputPrecio, string inputDate, string selectSexo)
        {
            Home home = new Home();
            if (selectRaza != null || selectProvincia != null || inputPoblacion != null || inputPrecio != null || inputDate != null || selectSexo != null)
            {
                home.Gallinas = this.ObtenerListadoGallinasConFiltro(loggedid, selectRaza, selectProvincia, inputPoblacion, inputPrecio, inputDate, selectSexo);
            }
            else
            {
                home.Gallinas = this.ObtenerListadoGallinas(loggedid);
            }
            home.Razas = this.ObtenerRazasGallinas();
            home.Provincias = this.ObtenerProvinciasGallinas();
            home.Blogs = this.ObtenerBlogsGallinas();
            return home;
        }

        public ActionResult Index(string selectRaza, string selectProvincia, string inputPoblacion, string inputPrecio, string inputDate, string selectSexo)
        {
            this.ActualizarSesion();
            int loggedid = 0;
            if (!String.IsNullOrEmpty(Session["logged"] as String))
            {
                loggedid = Convert.ToInt32(Session["loggedid"]);
            }

            Home home = this.ObtenerDatos(loggedid, selectRaza, selectProvincia, inputPoblacion, inputPrecio, inputDate, selectSexo);
            return View(home);
        }

        private void ActualizarSesion()
        {
            if (String.IsNullOrEmpty(Session["controller"] as String))
            {
                this.HttpContext.Session.Add("controller", "Home");
            }
            else
            {
                this.HttpContext.Session["controller"] = "Home";
            }
            if (String.IsNullOrEmpty(Session["view"] as String))
            {
                this.HttpContext.Session.Add("view", "Index");
            }
            else
            {
                this.HttpContext.Session["view"] = "Index";
            }
        }

        private List<Gallina> ObtenerListadoGallinasConFiltro(int loggedid, string selectRaza, string selectProvincia, string inputPoblacion, string inputPrecio, string inputDate, string selectSexo)
        {
            GallinaController gallinaController = new GallinaController();
            string sql = "SELECT g.*, r.raza, u.nombre nombreUsuario, u.foto fotoUsuario, p.Provincia";
            sql += " FROM gallina g";
            sql += " left join raza r on g.idRaza=r.idRaza";
            sql += " left join usuario u on u.idUsuario = g.idUsuario";
            sql += " left join provincias p on p.idProvincias = u.idProvincias";
            sql += " where g.ventas=0";
            if (!string.IsNullOrEmpty(selectRaza))
            {
                if (selectRaza != "Todas")
                {
                    sql += " and r.raza='" + selectRaza + "'";
                }
            }
            if (!string.IsNullOrEmpty(selectProvincia))
            {
                if (selectProvincia != "Todas")
                {
                    sql += " and p.Provincia='" + selectProvincia + "'";
                } 
            }
            //if (!string.IsNullOrEmpty(inputPoblacion))
            //{
            //    sql += " and r.raza='" + inputPoblacion + "'";
            //}
            if (!string.IsNullOrEmpty(inputPrecio))
            {
                sql += " and g.precio<='" + inputPrecio + "'";
            }
            if (!string.IsNullOrEmpty(inputDate))
            {
                sql += " and g.fechaAlta='" + inputDate + "'";
            }
            if (!string.IsNullOrEmpty(selectSexo))
            {
                if (selectSexo != "Todos")
                {
                    sql += " and g.sexo='" + selectSexo + "'";
                }
            }
            List<Object> gallinasObj = gallinaController.getAllGallinaHome(sql);

            return gallinaController.ConvertList<Gallina>(gallinasObj);
        }

        public ActionResult Filtro(string selectRaza, string selectProvincia, string inputPoblacion, string inputPrecio, string inputDate, string selectSexo)
        {
            return RedirectToAction("Index", "Home", new { selectRaza, selectProvincia, inputPoblacion, inputPrecio, inputDate, selectSexo });
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();

           
        }

    }
}