﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class FavoritoController : BaseController
    {
        // GET: Favorito
        public ActionResult Index()
        {
            return View();
        }

        public FavoritoController() : base()
        {
            this._model = new Favorito();
            this._table = "favorito";
        }

        public bool ConsultarFavorito(int idUsuario, int idGallina)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "SELECT *";
                sql += " FROM favorito f";
                sql += " WHERE f.idUsuario = " + idUsuario + " and f.idGallina = " + idGallina + ";";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                bool resultado = false;
                if (rdr.Read())
                {
                    resultado = true;
                }
                rdr.Close();
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return false;
            }
        }

        public ActionResult MarcarFavorito(int idUsuario, int idGallina, string callingView)
        {
            bool favorito = this.ConsultarFavorito(idUsuario, idGallina);
            if (!favorito)
            {
                this.Insertar(idUsuario, idGallina);
            }
            else
            {
                this.Eliminar(idUsuario, idGallina);
            }
            if (callingView == "Favoritos")
            {
                return RedirectToAction("Favoritos", "Usuario", new { idUsuario });
            }
            else if (callingView == "Index")
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Home");
        }

        private void Eliminar(int idUsuario, int idGallina)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "DELETE FROM favorito";
                sql += " WHERE idGallina=" + idGallina + " and idUsuario=" + idUsuario + ";";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        private void Insertar(int idUsuario, int idGallina)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "INSERT INTO favorito (idGallina, idUsuario)";
                sql += " VALUES (" + idGallina + ", " + idUsuario + ");";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        public override Object ToModel(MySqlDataReader rdr)
        {
            Favorito Favorito = new Favorito();
            Favorito.IdFavorito = Convert.ToInt32(rdr["idFavorito"]);
            Favorito.IdGallina = Convert.ToInt32(rdr["idGallina"]);         
            Favorito.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);          
            return Favorito; 
        }
    }
}