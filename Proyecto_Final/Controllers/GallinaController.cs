﻿using MySql.Data.MySqlClient;
using Proyecto_Final.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_Final.Controllers
{
    public class GallinaController: BaseController
    {

        public ActionResult Commit()
        {
            return View();
        }

        public GallinaController() : base()
        {
           this._model = new Gallina();
           this._table = "gallina";
        }//OK

        public List<Object> ObtenerGallinasFavoritas(int idUsuario)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "SELECT g.*, r.raza, u.nombre nombreUsuario, u.foto fotoUsuario, p.Provincia";
                sql += " FROM favorito f";
                sql += " left join gallina g on f.idGallina=g.idGallina";
                sql += " left join raza r on g.idRaza=r.idRaza";
                sql += " left join usuario u on u.idUsuario=g.idUsuario";
                sql += " left join provincias p on p.idProvincias=u.idProvincias";
                sql += " where g.ventas=0 and f.idUsuario=" + idUsuario + ";";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> resultado = new List<object>();
                while (rdr.Read())
                {
                    resultado.Add(this.ToModelGallinaHome(rdr));
                }
                rdr.Close();
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        private List<Gallina> ConsultarFavorito(List<Gallina> gallinas, int loggedid)
        {
            if (loggedid==0)
            {
                
            }
            else { FavoritoController favoritoController = new FavoritoController();
                foreach (Gallina gallina in gallinas)
                {
                    if (favoritoController.ConsultarFavorito(loggedid, gallina.IdGallina))
                    {
                        gallina.Favorito = "/images/icons8-heart-red-32.png";
                    }
                    else
                    {
                        gallina.Favorito = "/images/icons8-heart-outline-32.png";
                    }
                }}
            return gallinas;
        }

        public List<Gallina> ObtenerGallinas(int loggedid, int idUsuario)
        {
            string sql = "SELECT g.*, r.raza, u.nombre nombreUsuario, u.foto fotoUsuario, p.Provincia";
            sql += " FROM gallina g";
            sql += " left join raza r on g.idRaza=r.idRaza";
            sql += " left join usuario u on u.idUsuario = g.idUsuario";
            sql += " left join provincias p on p.idProvincias = u.idProvincias";
            sql += " where g.idUsuario='" + idUsuario + "' and g.ventas=0;";
            List<Object> gallinasObj = this.getAllGallinaHome(sql);
            List<Gallina> gallinas = this.ConvertList<Gallina>(gallinasObj);
            return this.ConsultarFavorito(gallinas, loggedid);
        }

        public List<Gallina> ObtenerGallinas(int loggedid)
        {
            string sql = "SELECT g.*, r.raza, u.nombre nombreUsuario, u.foto fotoUsuario, p.Provincia";
            sql += " FROM gallina g";
            sql += " left join raza r on g.idRaza=r.idRaza";
            sql += " left join usuario u on u.idUsuario = g.idUsuario";
            sql += " left join provincias p on p.idProvincias = u.idProvincias";
            sql += " where g.ventas=0;";
            List<Object> gallinasObj = this.getAllGallinaHome(sql);
            List<Gallina> gallinas = this.ConvertList<Gallina>(gallinasObj);
            return this.ConsultarFavorito(gallinas, loggedid);
        }

        public List<Object> getAllGallinaHome(string query = null)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = (query == null) ? string.Format("SELECT * FROM {0} limit 100", _table) : query;
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> resultado = new List<Object>();
                while (rdr.Read())
                {
                    resultado.Add(ToModelGallinaHome(rdr));
                }
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public Object ToModelGallinaHome(MySqlDataReader rdr)
        {
            Gallina gallina = new Gallina();
            gallina.IdGallina = Convert.ToInt32(rdr["idgallina"]);
            gallina.IdRaza = Convert.ToInt32(rdr["idRaza"]);
            gallina.Raza = rdr["Raza"].ToString();
            gallina.Nombre = rdr["nombre"].ToString();
            gallina.Dimensiones = rdr["Dimensiones"].ToString();
            gallina.Peso = Convert.ToDouble(rdr["peso"]);
            gallina.Sexo = rdr["sexo"].ToString();
            gallina.Precio = Convert.ToDouble(rdr["precio"]);
            gallina.Views = Convert.ToInt32(rdr["views"]);
            gallina.Fecha_Alta = Convert.ToDateTime(rdr["fechaAlta"]);
            gallina.HuevosGallina = Convert.ToInt32(rdr["huevosGallina"]);
            gallina.AudioGallo = rdr["audioGallo"].ToString();
            gallina.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);
            gallina.NombreUsuario = rdr["nombreUsuario"].ToString();
            gallina.Ventas = Convert.ToBoolean(rdr["ventas"]);

            gallina.Fotos = AllFotos(gallina.IdGallina);
            gallina.Foto = gallina.Fotos[0];

            gallina.Localidad = rdr["Provincia"].ToString();
            gallina.FotoUsuario = rdr["fotoUsuario"].ToString();
            

            return gallina;
        }

        public override Object ToModel(MySqlDataReader rdr)
        {
            Gallina gallina = new Gallina();
            gallina.IdGallina = Convert.ToInt32(rdr["idGallina"]);
            gallina.IdRaza = Convert.ToInt32(rdr["idRaza"]);
            gallina.Raza = rdr["Raza"].ToString();
            gallina.Nombre = rdr["nombre"].ToString();
            gallina.Dimensiones = rdr["Dimensiones"].ToString();
            gallina.Peso = Convert.ToDouble(rdr["peso"].ToString());//Convierto primero a string lo devuelto por la base de datos para que c# pueda convertirlo bien a double
            gallina.Sexo = rdr["sexo"].ToString();
            gallina.Precio = Convert.ToDouble(rdr["precio"]);
            gallina.Views = Convert.ToInt32(rdr["views"]);
            gallina.Fecha_Alta = Convert.ToDateTime(rdr["fechaAlta"]);
            gallina.HuevosGallina = Convert.ToInt32(rdr["huevosGallina"]);
            gallina.AudioGallo = rdr["audioGallo"].ToString();
            gallina.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);
            gallina.NombreUsuario = rdr["nombreUsuario"].ToString();
            gallina.Ventas = Convert.ToBoolean(rdr["ventas"]);

            gallina.Fotos = AllFotos(gallina.IdGallina);
            gallina.Foto = gallina.Fotos[0];
            gallina.Descripcion = rdr["descripcion"].ToString();

            gallina.Views = Convert.ToInt32(rdr["views"]);
            gallina.Localidad = rdr["localidad"].ToString();
            gallina.ValorHuevos = Convert.ToInt32(rdr["huevosGallina"]);
            gallina.Audio = rdr["audioGallo"].ToString();
            gallina.Raza = this.NombreRaza(gallina.IdRaza);
            gallina.Tags = this.UpdateTags(gallina.IdGallina);
            
            string[] usuario = new string[2];
            usuario = this.AllUsuario(gallina.IdGallina);
            gallina.NomUsuario = usuario[0];
            gallina.FotUsuario = usuario[1];

            gallina.Fecha = String.Format(gallina.Fecha_Alta.Day.ToString()+"/"+ gallina.Fecha_Alta.Month.ToString() + "/" + gallina.Fecha_Alta.Year.ToString() );

            gallina.FotoUsuario = rdr["fotoUsuario"].ToString();
            

            return gallina; 
        }//OK

        public  Gallina TooModel(MySqlDataReader rdr)
        {
            Gallina gallina = new Gallina();
            gallina.IdGallina = Convert.ToInt32(rdr["idGallina"]);
            gallina.IdRaza = Convert.ToInt32(rdr["idRaza"]);
            gallina.Nombre = rdr["nombre"].ToString();
            gallina.Dimensiones = String.Format(rdr["dimensiones"].ToString()+" cm");
            gallina.Precio = Convert.ToDouble(rdr["precio"].ToString());
            gallina.Peso = Convert.ToDouble(rdr["peso"].ToString());
            gallina.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);
            gallina.Fecha_Alta = Convert.ToDateTime(rdr["fechaAlta"]);
            gallina.Descripcion = rdr["descripcion"].ToString();
            gallina.Fotos = this.AllFotos(gallina.IdGallina);
            gallina.Foto = gallina.Fotos[0];
            gallina.Views = Convert.ToInt32(rdr["views"]);
            gallina.Localidad = rdr["localidad"].ToString();
            gallina.ValorHuevos = Convert.ToInt32(rdr["huevosGallina"]);
            gallina.Audio = rdr["audioGallo"].ToString();
            gallina.Raza = this.NombreRaza(gallina.IdRaza);
            gallina.Tags = this.UpdateTags(gallina.IdGallina);
            gallina.Sexo = rdr["sexo"].ToString();

            string[] usuario = new string[2];
            usuario = this.AllUsuario(gallina.IdGallina);
            gallina.NomUsuario = usuario[0];
            gallina.FotUsuario = usuario[1];

            gallina.Fecha = String.Format(gallina.Fecha_Alta.Day.ToString() + "/" + gallina.Fecha_Alta.Month.ToString() + "/" + gallina.Fecha_Alta.Year.ToString());

            return gallina;
        }//OK

        public ActionResult Detail(int id)
        {
            this.AumentarVisitas(id);
            Gallina gallina = new Gallina();
            try
            {
                MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                             //Creamos un string con las siguientes indicaciones.
                string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                _conn = new MySqlConnection(connStr);
                _conn.Open();



                string sql = string.Format("select * from gallina where idgallina={0} ;", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                
                if (rdr.Read())
                {
                    
                    gallina = this.TooModel(rdr);



                    
                }
                
                
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally//siempre llegamos aqui ocurra lo que ocurra
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }
            }
            return View(gallina);

        } //OK
        
        public ActionResult Create()
        {
            this.SelectRazas();
            this.SelectLocalidad();
            return View();
        }//OK
        
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
           
            try
            {

                Gallina gallina = new Gallina();
                gallina.IdGallina = Convert.ToInt32(collection["idGallina"]);
                gallina.IdRaza = Convert.ToInt32(collection["idRaza"]);
                gallina.Nombre = collection["Nombre"].ToString();
                gallina.Dimensiones = collection["Dimensiones"].ToString();                
                string precio = collection["Precio"];
                gallina.Precio =Double.Parse(precio,System.Globalization.CultureInfo.InvariantCulture);
                string peso = collection["Peso"];
                gallina.Peso = Double.Parse(peso, System.Globalization.CultureInfo.InvariantCulture);
                gallina.Localidad = collection["localidad"].ToString();
               

                string sexo = collection["sexo"].ToString();

                if (sexo.Equals("Macho")) gallina.Sexo = "Macho";
                else { gallina.Sexo = "Hembra"; }
                                         
                gallina.Descripcion = collection["descripcion"].ToString();
                gallina.IdUsuario = Convert.ToInt32(Session["loggedid"]);
                gallina.Views = Convert.ToInt32(collection["views"]);
                gallina.ValorHuevos =Convert.ToInt32(collection["valorHuevos"]) ;

                    MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                                 //Creamos un string con las siguientes indicaciones.
                    string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                    _conn = new MySqlConnection(connStr);
                    _conn.Open();



                    string sql = string.Format("insert into gallina (idRaza,nombre,dimensiones,peso,sexo,idUsuario,descripcion" +
                        ",huevosGallina,audioGallo,ventas,views,localidad,precio)values" +
                        "(@idRaza , @nombre ,  @dimensiones ,@peso ,@sexo,@idUsuario ,@descripcion ,@huevosGallina,@audioGallo,@ventas,@views,@localidad,@precio)");
                    MySqlCommand cmd = new MySqlCommand(sql, _conn);

                    cmd.Parameters.AddWithValue("@nombre", gallina.Nombre);
                    cmd.Parameters.AddWithValue("@idRaza", gallina.IdRaza);
                    cmd.Parameters.AddWithValue("@dimensiones", gallina.Dimensiones);
                    cmd.Parameters.AddWithValue("@precio", gallina.Precio);
                    cmd.Parameters.AddWithValue("@peso", gallina.Peso);
                    cmd.Parameters.AddWithValue("@sexo", gallina.Sexo);
                    cmd.Parameters.AddWithValue("@idUsuario", gallina.IdUsuario);
                    cmd.Parameters.AddWithValue("@descripcion", gallina.Descripcion);
                    cmd.Parameters.AddWithValue("@huevosGallina", gallina.ValorHuevos);
                    HttpPostedFileBase file = Request.Files[3];
                    cmd.Parameters.AddWithValue("@audioGallo", this.GuardarAudio(file,gallina.Nombre));
                    cmd.Parameters.AddWithValue("@ventas", 0);
                    cmd.Parameters.AddWithValue("@views", gallina.Views);
                    cmd.Parameters.AddWithValue("@localidad", gallina.Localidad);

                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                    _conn.Close();


                HttpPostedFileBase[] fotos = new HttpPostedFileBase[3];
                fotos[0] = Request.Files[0];
                fotos[1] = Request.Files[1];
                fotos[2] = Request.Files[2];
                int id = this.IdGallina();
                gallina.Tags = this.Tags(collection, id);
                if (id!=0)
                {
                    this.GuardarFotos(fotos, gallina.Nombre, id);
                }





                int ide = id;
                return RedirectToAction("Detail", "Gallina", new { id = ide });

            }
            catch(Exception e)
            {
                return RedirectToAction("Index");
            }
        }//OK

        public ActionResult Borrar(int id)
        {
            Gallina gallina = this.GetIdGallina(id);
            return View(gallina);
        }//OK
        [HttpPost]
        public ActionResult Borrar(FormCollection collection, int id )
        {
            Gallina gallina = new Gallina();
            gallina.IdGallina = Convert.ToInt32(collection["IdGallina"]);

            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();
            try
            {
                string sql = string.Format("delete from gallinahastags where idGallina={0};", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _conn.Close();

            }
            try
            {
                string sql = string.Format("delete from fotosgallina where idGallina={0};", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Prepare();
                cmd.ExecuteNonQuery();               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _conn.Close();
               
            }

            try
            {
                string sql = string.Format("delete from gallina where idGallina ={0};", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();

                return RedirectToAction("Commit");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _conn.Close();
                return View();
            }
          
        }//OK

        public ActionResult Editar(int id)
        {
            this.SelectRazas();
            this.SelectLocalidad();
            Gallina gallina = this.GetIdGallina(id);
            return View(gallina);
        }

        [HttpPost]
        public ActionResult Editar(FormCollection collection , int id)
        {
            try
            {

                Gallina gallina = new Gallina();
                gallina.IdGallina = Convert.ToInt32(collection["idGallina"]);
                gallina.IdRaza = Convert.ToInt32(collection["idRaza"]);
                gallina.Nombre = collection["Nombre"].ToString();
                gallina.Dimensiones = collection["Dimensiones"].ToString();
                string precio = collection["Precio"];
                gallina.Precio = Double.Parse(precio, System.Globalization.CultureInfo.InvariantCulture);
                string peso = collection["Peso"];
                gallina.Peso = Double.Parse(peso, System.Globalization.CultureInfo.InvariantCulture);
                gallina.Localidad = collection["localidad"].ToString();


                string sexo = collection["sexo"].ToString();

                if (sexo.Equals("Macho")) gallina.Sexo = "Macho";
                else { gallina.Sexo = "Hembra"; }

                gallina.Descripcion = collection["descripcion"].ToString();
                gallina.IdUsuario = Convert.ToInt32(Session["loggedid"]);
                gallina.Views = Convert.ToInt32(collection["views"]);
                gallina.ValorHuevos = Convert.ToInt32(collection["valorHuevos"]);

                MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                             //Creamos un string con las siguientes indicaciones.
                string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                _conn = new MySqlConnection(connStr);
                _conn.Open();



                string sql = string.Format("update gallina set idRaza=@idRaza,nombre=@nombre,dimensiones=@dimensiones,peso=@peso,sexo=@sexo," +
                    "descripcion=@descripcion,huevosGallina=@huevosgallina,audioGallo=@audiogallo,localidad=@localidad,precio=@precio where idGallina = {0}",id );
                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@nombre", gallina.Nombre);
                cmd.Parameters.AddWithValue("@idRaza", gallina.IdRaza);
                cmd.Parameters.AddWithValue("@dimensiones", gallina.Dimensiones);
                cmd.Parameters.AddWithValue("@precio", gallina.Precio);
                cmd.Parameters.AddWithValue("@peso", gallina.Peso);
                cmd.Parameters.AddWithValue("@sexo", gallina.Sexo);
                cmd.Parameters.AddWithValue("@idUsuario", gallina.IdUsuario);
                cmd.Parameters.AddWithValue("@descripcion", gallina.Descripcion);
                cmd.Parameters.AddWithValue("@huevosGallina", gallina.ValorHuevos);
                if (Request.Files[3].FileName.ToString()!="")
                {
                    HttpPostedFileBase file = Request.Files[3];
                    cmd.Parameters.AddWithValue("@audioGallo", this.GuardarAudio(file, gallina.Nombre));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@audioGallo", "null");
                }                            
                cmd.Parameters.AddWithValue("@localidad", gallina.Localidad);

                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();


                HttpPostedFileBase[] fotos = new HttpPostedFileBase[3];
                fotos[0] = Request.Files[0];
                fotos[1] = Request.Files[1];
                fotos[2] = Request.Files[2];
               
                gallina.Tags = this.Tags(collection, id);
                

                this.UpdateFotos(fotos, gallina.Nombre, id);

            }
            catch (Exception e)
            {
                Console.WriteLine();
                return RedirectToAction("Index");
            }
            int ide = id;
            return RedirectToAction("Detail","Gallina",new { id=ide});
            
                             /*    return RedirectToAction("ACTION", "CONTROLLER", new {
                               id = 99, otherParam = "Something", anotherParam = "OtherStuff" 
                           });
                    Entonces la url sería:

                        /CONTROLLER/ACTION/99?otherParam=Something&anotherParam=OtherStuff
                    A continuación, su controlador puede hacer referencia a estos:

                    public ActionResult ACTION(string id, string otherParam, string anotherParam) {
                       // Your code
          }*/
        }

        public void GuardarFotos(HttpPostedFileBase [] file , string nombre,int idgallina)
        {
            Random rmd = new Random();

            int x = rmd.Next(0, 3000);
            string [] db_path = new string [3];
            int contador = 0;
            foreach (HttpPostedFileBase i in file)
            {
                if (i != null && i.FileName.Length>0)//si no hay foto FileName esta vacio
                {
                    //ojo con posibles espacios en blanco ...
                    string pic = x.ToString() + nombre +  System.IO.Path.GetFileName(file[contador].FileName);
                    string path = System.IO.Path.Combine(Server.MapPath("~/Images/upload"), pic);
                    db_path[contador] = "/Images/upload/" + pic;
                    // file is uploaded
                    i.SaveAs(path);
                }
                else
                {
                    db_path[contador] = "/Images/fondo.png";
                }
                x = rmd.Next(0, 3000);
                contador++;
            }
          

            FotosGallina pers = new FotosGallina();
            pers.Foto1 = (db_path[0]==null) ? "/Images/fondo.png" : db_path[0] ;
            pers.Foto2 = (db_path[1]==null) ? "/Images/fondo.png" : db_path[1] ;
            pers.Foto3 = (db_path[2]==null) ? "/Images/fondo.png" : db_path[2] ;


            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {


                string sql = string.Format("insert into fotosgallina (foto1,foto2,foto3,idGallina)values" +
                    "( @foto1 , @foto2 , @foto3 ,@idGallina)");
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Parameters.AddWithValue("@foto1", pers.Foto1);
                cmd.Parameters.AddWithValue("@foto2", pers.Foto2);
                cmd.Parameters.AddWithValue("@foto3", pers.Foto3);
                cmd.Parameters.AddWithValue("@idGallina",idgallina);


                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();

                
      
            }
            catch (Exception e)
            {
                Console.WriteLine("Fotos no guardadas");
            }
        }//OK

        public void UpdateFotos(HttpPostedFileBase[] file, string nombre, int idgallina)
        {
            Random rmd = new Random();

            int x = rmd.Next(0, 3000);
            string[] db_path = new string[3];
            int contador = 0;
            foreach (HttpPostedFileBase i in file)
            {
                if (i != null && i.FileName.Length > 0)//si no hay foto FileName esta vacio
                {
                    //ojo con posibles espacios en blanco ...
                    string pic = x.ToString() + nombre + System.IO.Path.GetFileName(file[contador].FileName);
                    string path = System.IO.Path.Combine(Server.MapPath("~/Images/upload"), pic);
                    db_path[contador] = "/Images/upload/" + pic;
                    // file is uploaded
                    i.SaveAs(path);
                }
                else
                {
                    db_path[contador] = "/Images/fondo.png";
                }
                x = rmd.Next(0, 3000);
                contador++;
            }


            FotosGallina pers = new FotosGallina();
            pers.Foto1 = (db_path[0] == null) ? "/Images/fondo.png" : db_path[0];
            pers.Foto2 = (db_path[1] == null) ? "/Images/fondo.png" : db_path[1];
            pers.Foto3 = (db_path[2] == null) ? "/Images/fondo.png" : db_path[2];


            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {

                string sql = string.Format("update fotosgallina set foto1=@foto1,foto2=@foto2,foto3=@foto3 where idGallina = {0}",idgallina);
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Parameters.AddWithValue("@foto1", pers.Foto1);
                cmd.Parameters.AddWithValue("@foto2", pers.Foto2);
                cmd.Parameters.AddWithValue("@foto3", pers.Foto3);              

                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Fotos no guardadas");
            }
        }//OK

        public int IdGallina()
        {
            int id = 0;
            try
            {
                MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                             //Creamos un string con las siguientes indicaciones.
                string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                _conn = new MySqlConnection(connStr);
                _conn.Open();

                

                string sql = string.Format("select last_insert_id() as ultimo");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    id = Convert.ToInt32(rdr["ultimo"]);
                }

                _conn.Close();
                return id;
            }
            catch ( Exception e)
            {
                Console.WriteLine();
                return id;
            }finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }
                
            }
           
        }//OK

        public string GuardarAudio(HttpPostedFileBase file , string nombre )
        {
            Random rmd = new Random();

            int x = rmd.Next(0, 3000);

            string db_path = "null";
                    
                if (file != null && file.FileName.Length > 0)//si no hay audio FileName esta vacio
                {
                    //ojo con posibles espacios en blanco ...
                    string pic = "audio"+ x.ToString() + nombre + System.IO.Path.GetFileName(file.FileName);
                    string path = System.IO.Path.Combine(Server.MapPath("~/Sounds"), pic);
                    db_path = "/Sounds/" + pic;
                    // file is uploaded
                    file.SaveAs(path);
                }
            return db_path;
        }//OK

        public void SelectRazas()
        {
            RazaController craza = new RazaController();
            List<Raza> ll = craza.GetAll();
            List<SelectListItem> sl = new List<SelectListItem>();
            foreach (Raza r in ll)
            {
                SelectListItem item = new SelectListItem();
                item.Value = r.IdRaza.ToString();
                item.Text = r.Nombre;
                sl.Add(item);
            }

            ViewBag.Razas = sl;
        }//OK

        public void SelectLocalidad()
        {
            ProvinciaController craza = new ProvinciaController();
            List<Provincia> ll = craza.GetAll();
            List<SelectListItem> sl = new List<SelectListItem>();
            foreach (Provincia r in ll)
            {
                SelectListItem item = new SelectListItem();
                item.Value = r.Nombre.ToString();
                item.Text = r.Nombre;
                sl.Add(item);
            }

            ViewBag.Localidad = sl;
        }//OK

        public string[] Tags(FormCollection collection , int idgallina)
        {
            string[] tags = new string[3];

            tags[0] = String.Format("#" + collection["tag1"].ToString());
            if (this.GuardaTag(tags[0]))
            {
                int idtag = this.IdGallina();
                this.GuardaHasTag(idtag,idgallina);
                tags[1] = String.Format("#" + collection["tag2"].ToString());
                if (this.GuardaTag(tags[1]))
                {
                    idtag = this.IdGallina();
                    this.GuardaHasTag(idtag, idgallina);

                    tags[2] = String.Format("#" + collection["tag3"].ToString());
                    if (this.GuardaTag(tags[2]))
                    {
                         idtag = this.IdGallina();
                        this.GuardaHasTag(idtag, idgallina);
                        return tags;
                    }
                    else { return tags; }
                }
                else { return tags; }

            }
            else { return tags; }
           
        }//OK

        public string[] UpdateTags(int id)
        {
            MySqlConnection _conn = null;
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";
            string[] tags = new string[3];
            int i = 0;
            try
            {
                _conn = new MySqlConnection(connStr);

                _conn.Open();
                string sql = string.Format("select ta.tag from tags ta where idTags in(SELECT idTags FROM gallinas.gallinahastags " +
                    "where idGallina = {0} order by idTags desc )group by ta.idTags desc limit 3;", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();


                while (rdr.Read())
                {
                    if (rdr["tag"].ToString()=="")
                    {
                        tags[i] = "#";
                        i++;
                    }
                    else
                    {
                        tags[i] = rdr["tag"].ToString();
                        i++;
                    }
                    
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return tags;

        }//Actualiza a los tres ultimos tags ingresados


        public bool GuardaTag(string tag)
        {
            bool error = false;
            try
            {
                MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                             //Creamos un string con las siguientes indicaciones.
                string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                _conn = new MySqlConnection(connStr);
                _conn.Open();



                string sql = string.Format("INSERT INTO tags (tag) VALUES(@tag1);" );

                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@tag1", tag);
          
                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();

                error = true;
                return error;
            }
            catch (Exception ex)
            {

                Console.WriteLine("error");
                _conn.Close();
                return error;
            }
        }//OK

        public void GuardaHasTag(int idtag , int idgallina)
        {
           
            try
            {
                MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                             //Creamos un string con las siguientes indicaciones.
                string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                _conn = new MySqlConnection(connStr);
                _conn.Open();



                string sql = string.Format("INSERT INTO gallinahastags (idGallina ,idTags) VALUES(@idgallina,@idtag);");

                MySqlCommand cmd = new MySqlCommand(sql, _conn);

                cmd.Parameters.AddWithValue("@idgallina", idgallina);
                cmd.Parameters.AddWithValue("@idtag", idtag);

                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();

              
                
            }
            catch (Exception ex)
            {

                Console.WriteLine("error");
                _conn.Close();
                
            }
        }//OK

        public string NombreRaza(int id)
        {
            string raza = "";
            try
            {
                MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                             //Creamos un string con las siguientes indicaciones.
                string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                _conn = new MySqlConnection(connStr);
                _conn.Open();



                string sql = string.Format("SELECT raza from raza where idRaza = {0};",id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    raza = (rdr["raza"].ToString());
                }

                _conn.Close();
                return raza;
            }
            catch (Exception e)
            {
                Console.WriteLine();
                return raza;
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }

            }

        }//OK

        public string[] AllUsuario(int id )
        {
            MySqlConnection _conn = null;
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";
            string[] usuario = new string[3];
           
            try
            {
                _conn = new MySqlConnection(connStr);

                _conn.Open();
                string sql = string.Format("SELECT u.nombre , u.foto FROM usuario u inner join gallina g on g.idUsuario = u.idUsuario where g.idGallina = {0} ; ", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();


                if (rdr.Read())
                {
                    usuario[0] = rdr["nombre"].ToString();
                    usuario[1] = rdr["foto"].ToString();
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return usuario;
        }//OK

        public Gallina GetIdGallina(int id)
        {
            Object gallina = new Object();
            try
            {
                MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                             //Creamos un string con las siguientes indicaciones.
                string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

                _conn = new MySqlConnection(connStr);
                _conn.Open();



                string sql = string.Format("select * from gallina where idgallina={0} ;", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();


                if (rdr.Read())
                {

                    gallina = this.TooModel(rdr);




                }


            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally//siempre llegamos aqui ocurra lo que ocurra
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }
            }
            return (Gallina)gallina;
        }//OK

        public string[] AllFotos(int id)
        {
            MySqlConnection _conn = null;
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";
            string[] fotos = new string[3];
            try//bloque try para controlar las excepciones.
            {
                _conn = new MySqlConnection(connStr);//utilizamos el constructor para crear el objeto de conexion

                string tabla = "fotosgallina";
                _conn.Open();//abrimos la sesion con el servidor SQl
                string sql = string.Format("SELECT foto1,foto2,foto3 FROM {0} where idgallina = {1}", tabla, id);//metemos la consulta SQL en un string 
                MySqlCommand cmd = new MySqlCommand(sql, _conn);//le pasamos la consulta y el obejo que habre la conexion
                MySqlDataReader rdr = cmd.ExecuteReader();//este objeto nos devuelve los datos que hemos consultado 

                if (rdr.Read())//se situa en la primera posicion si hay datos entra en el if sino hay datos devovera falso
                {
                    fotos[0] = rdr["foto1"].ToString();
                    fotos[1] = rdr["foto2"].ToString();//dentro de los corchetes escribimos exactamente el nombre de los campos de la base de datos
                    fotos[2] = rdr["foto3"].ToString();
                }
                rdr.Close();//cerramos el evento rdr

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally//siempre llegamos aqui ocurra lo que ocurra
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }
            }
            return fotos;//devuelve un objeto con los datos de la base de datos

        }//Lista de la fotos de gallina

        public string[] AllTags(int id)
        {
            MySqlConnection _conn = null;
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";
            string[] tags = new string[3];
            int i = 0;
            try
            {
                _conn = new MySqlConnection(connStr);

                _conn.Open();
                string sql = string.Format("select ta.tag  from tags ta inner join gallinahastags gh on ta.idTags = gh.idTags where idGallina = {0} ; ", id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();


                while (rdr.Read())
                {
                    tags[i] = rdr["tag"].ToString();
                    i++;
                }
                rdr.Close();

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)
                {
                    _conn.Close();
                }
            }
            return tags;

        }//Lista de los tags guardados en gallina

        public void AumentarVisitas(int idgallina)
        {

            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();
            
            try
            {


                string sql = string.Format("update gallina set views =(views+1) where idGallina={0};",idgallina);
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = _conn;               
                
                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();



            }
            catch (Exception e)
            {
                Console.WriteLine("Fotos no guardadas");
                _conn.Close();
            }
        }

    }
}   
        