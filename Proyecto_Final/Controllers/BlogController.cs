﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class BlogController : BaseController
    {
        private static int contadorViews = 0;

        public BlogController() : base()
        {
            this._model = new Blog();
            this._table = "blog";
            
        }

        // GET: Blog
        public ActionResult Index()
        {
            /*cambiar listado de objectos para listado del blog en this.getall */
            List<Object> obj = this.getAll();
            //ViewData["Index"] =  ;
            return View(this.ConvertList<Blog>(obj));
        }
        public ActionResult Lista()
        {
            List<Object> obj = this.getAll();
            return View(this.ConvertList<Blog>(obj));
        }

        public ActionResult Blog(int idBlog)
        {
            ++contadorViews;
            Infoblog info = new Infoblog();
            info.Blog = (Blog) getBlogbyId(idBlog);
            info.Blog.Views = contadorViews;
            info.Comentarios = ObtenerListadoComentarios(idBlog);
            return View(info);
        }

        private List<ComentarioBlog> ObtenerListadoComentarios(int idUsuario)
        {
            ComentarioBlogController comentarioblogcontroller = new ComentarioBlogController(); 
            List<Object> comentariosObj = comentarioblogcontroller.ObtenerComentarios(idUsuario);
            return comentarioblogcontroller.ConvertList<ComentarioBlog>(comentariosObj);
        }
        

        public override Object ToModel(MySqlDataReader rdr)
        {
            Blog Blog = new Blog();
            Blog.IdBlog = Convert.ToInt32(rdr["idBlog"]);
            Blog.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);
            Blog.Titulo = rdr["titulo"].ToString();
            Blog.Texto = rdr["texto"].ToString();
            Blog.Resumen = rdr["resumen"].ToString();
            Blog.Fecha = Convert.ToDateTime(rdr["fecha"]);
            Blog.Foto = rdr["foto"].ToString();
            Blog.Like =Convert.ToInt32(rdr["like"]);          
            Blog.DisLike = Convert.ToInt32(rdr["dislike"]);         
            Blog.Views = Convert.ToInt32(rdr["views"]);
            return Blog; 
        }

        public Object getBlogbyId(int id)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = string.Format("SELECT * FROM {0} WHERE IdBlog={1}", _table, id);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Read();
                Object ob = ToModel(rdr);
                rdr.Close();
                _conn.Close();
                return ob;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

    }
}