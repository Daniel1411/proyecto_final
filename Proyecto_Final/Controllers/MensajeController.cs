﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class MensajeController : BaseController
    {
        public ActionResult ListadoChat(int idUsuario)
        {
            List<Chat> ListadoDeChats = ObtenerListaChats(idUsuario);
            return View(ListadoDeChats);
        }

        public ActionResult ComprobarChat (int receptor , int gallina, string precio)
        {

            int IdUsuarioEmisor = Convert.ToInt32(Session["LoggedId"]);
            int idchat = 0;
            int IdUsuarioReceptor = Convert.ToInt32(receptor);
            int Idgallina = Convert.ToInt32(gallina);

            if (ChatCreado(IdUsuarioEmisor,IdUsuarioReceptor,ref idchat))
            {
                
                return RedirectToAction("Chat", "Mensaje", new { idchat = idchat});
            }
            else
            {
                this.CrearVenta(IdUsuarioEmisor, IdUsuarioReceptor, Idgallina, Convert.ToDouble(precio));
                return this.CreateChat(IdUsuarioReceptor);
            }
        }

        private void CrearVenta(int idComprador, int idVendedor, int idGallina, double valorVenta)
        {
            VentasController ventasController = new VentasController();
            ventasController.CrearVenta(idComprador, idVendedor, idGallina, valorVenta);

        }

        public bool ChatCreado(int IdUsuarioEmisor, int IdUsuarioReceptor,ref int idchat)
        {
            bool chatcreado = false;

            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {
                string sql = String.Format(" select idchat FROM chat WHERE(IdUsuarioEmisor={0} AND IdUsuarioReceptor={1}) OR (idUsuarioEmisor={2} AND idUsuarioReceptor={3}); ", IdUsuarioEmisor , IdUsuarioReceptor, IdUsuarioReceptor, IdUsuarioEmisor);           
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Chat> resultado = new List<Chat>();
                if (rdr.Read())
                {
                    idchat = Convert.ToInt32(rdr["idChat"]);
                    chatcreado = true;

                }
                else 
                {
                    return chatcreado;
                }
                _conn.Close();
                return chatcreado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return chatcreado;
            }

        }



        private List<Chat> ObtenerListaChats(int idUsuario)
        {
            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {
                string sql = "SELECT c.*, u.nombre receptor, u2.nombre emisor, u.foto fotoreceptor, u2.foto fotoemisor  ";
                sql += " FROM chat c";
                sql += " LEFT JOIN usuario u";
                sql += " on u.idUsuario=c.idUsuarioReceptor ";
                sql += " LEFT JOIN usuario u2";
                sql += " on u2.idUsuario=c.idUsuarioEmisor ";
                sql += " WHERE c.idUsuarioEmisor=" +idUsuario+ " or  c.idUsuarioReceptor=" + idUsuario + ";";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Chat> resultado = new List<Chat>();
                while (rdr.Read())
                {
                    resultado.Add(ToChat(rdr));
                }
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }


        private Chat ObtenerChat(int idchat)
        {
            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {
                string sql = "SELECT c.*, u.nombre receptor, u2.nombre emisor, u.foto fotoreceptor, u2.foto fotoemisor ";
                sql += " FROM chat c";
                sql += " LEFT JOIN usuario u";
                sql += " on u.idUsuario=c.idUsuarioReceptor ";
                sql += " LEFT JOIN usuario u2";
                sql += " on u2.idUsuario=c.idUsuarioEmisor ";
                sql += " WHERE idChat = "+idchat;
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                Chat c=null;
                if (rdr.Read())
                {
                    c = ToChat(rdr);
                }
                _conn.Close();
                return c;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        private Chat ToChat(MySqlDataReader rdr)
        {
            Chat chat = new Chat();
            chat.IdChat = Convert.ToInt32(rdr["idChat"]);
            chat.IdUsuarioEmisor = Convert.ToInt32(rdr["idUsuarioEmisor"]);
            chat.IdUsuarioReceptor = Convert.ToInt32(rdr["idUsuarioReceptor"]);
            chat.NombreEmisor = rdr["emisor"].ToString();
            chat.NombreReceptor = rdr["receptor"].ToString();
            chat.FotoEmisor = rdr["fotoemisor"].ToString();
            chat.FotoReceptor = rdr["fotoreceptor"].ToString();

            //chat.Fecha = Convert.ToDateTime(rdr["fecha"]);          
            return chat;
        }

        public ActionResult Chat(int idchat)
        {
            Chat cht = (Chat) this.ObtenerChat(idchat);
            int usuari_actual = Convert.ToInt32(Session["loggedid"]);
            
            if (usuari_actual==cht.IdUsuarioEmisor)
            {
                ViewData["elnom"] = cht.NombreReceptor;
                ViewData["lafoto"] = cht.FotoReceptor;

            }
            else
            {
                ViewData["elnom"] = cht.NombreEmisor;
                ViewData["lafoto"] = cht.FotoEmisor;

            }
            ViewData["idchat"] = idchat;
            ViewData["usuari_actual"] = usuari_actual;
            ViewData["emisor"] = cht.NombreEmisor;
            ViewData["receptor"] = cht.NombreReceptor;
            List<Mensaje> Model = this.getAll(idchat).Cast<Mensaje>().ToList();
            return View(Model);
        }
        public MensajeController() : base()
        {
           this._model = new Mensaje();
           this._table = "mensaje";
        }
        public  List<Object> getAll(int idchat)
        {
            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {
                string sql = string.Format("SELECT * FROM {0} where idchat={1}", _table, idchat);
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> resultado = new List<Object>();
                while (rdr.Read())
                {
                    resultado.Add(ToModel(rdr));
                }
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            string mensaje = collection["mensaje"];
            int IdChat = Convert.ToInt32(collection["idchat"]);
            int IdAutor = Convert.ToInt32(Session["loggedid"]);

            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {
                string sql = "INSERT INTO mensaje (idChat, texto, idAutor)";
                sql += " VALUES (@IdChat, @Texto, @IdAutor)";
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Parameters.AddWithValue("@IdChat", IdChat);
                cmd.Parameters.AddWithValue("@Texto", mensaje);
                cmd.Parameters.AddWithValue("@IdAutor", IdAutor);
                cmd.Prepare();
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
            return RedirectToAction("Chat", new { idchat = IdChat });
        }

        public ActionResult CreateChat(int idUsuarioReceptor)
        {
            this.CrearChatEnBD(idUsuarioReceptor);
            //obtener el ultimo id
            int idChat = this.ObtenerIdChat();
            return RedirectToAction("Chat", new { idchat = idChat });
        }

        private int ObtenerIdChat()
        {
            int IdChat = -1;
            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {
                string sql = string.Format("select last_insert_id() as ultimo");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    IdChat = Convert.ToInt32(rdr["ultimo"]);
                }

                _conn.Close();
                return IdChat;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return -1;
            }
        }

        private void CrearChatEnBD(int idUsuarioReceptor)
        {
            
            
            int loggedid = 0;
            if (!String.IsNullOrEmpty(Session["logged"] as string))
            {
                loggedid = Convert.ToInt32(Session["loggedid"]);
            }
            int IdEmisor = loggedid;
           // int IdChat = -1;
            MySqlConnection _conn = null;//ceramos un abjeto y lo inicializamos en null
                                         //Creamos un string con las siguientes indicaciones.
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            _conn = new MySqlConnection(connStr);
            _conn.Open();

            try
            {
                string sql = "INSERT INTO chat (idUsuarioEmisor, idUsuarioReceptor)";
                sql += " VALUES (@IdUsuarioEmisor, @IdUsuarioReceptor)";
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Parameters.AddWithValue("@IdUsuarioEmisor", IdEmisor);
                cmd.Parameters.AddWithValue("@IdUsuarioReceptor", idUsuarioReceptor);
                cmd.Prepare();
                cmd.ExecuteNonQuery();

                _conn.Close();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        public override Object ToModel(MySqlDataReader rdr)
        {
            Mensaje Mensaje = new Mensaje();
            Mensaje.IdMensaje = Convert.ToInt32(rdr["idMensaje"]);
            Mensaje.IdChat = Convert.ToInt32(rdr["idChat"]);
            Mensaje.Texto = rdr["texto"].ToString();
            Mensaje.IdAutor = Convert.ToInt32(rdr["idAutor"]);
            DateTime fecha = Convert.ToDateTime(rdr["fecha"]);
            Mensaje.Fecha = String.Format(fecha.Hour.ToString()+":"+fecha.Minute.ToString()+"   "+fecha.Day.ToString()+"/"+fecha.Month+"/"+fecha.Year.ToString());        
            return Mensaje; 
        }
    }
}