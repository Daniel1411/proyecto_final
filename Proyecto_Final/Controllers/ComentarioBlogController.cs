﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class ComentarioBlogController : BaseController
    {
        // GET: ComentarioBlog
        public ActionResult Index()
        {
            return View();
        }

         public ComentarioBlogController() : base()
        {
           this._model = new ComentarioBlog();
           this._table = "comentarioblog";
        }


        public override Object ToModel(MySqlDataReader rdr)
        {
            ComentarioBlog ComentarioBlog = new ComentarioBlog();
            ComentarioBlog.IdComentarioBlog = Convert.ToInt32(rdr["idComentarioBlog"]);
            ComentarioBlog.IdBlog = Convert.ToInt32(rdr["idBlog"]);
            ComentarioBlog.IdUsuario = Convert.ToInt32(rdr["idBlog"]);
            ComentarioBlog.Nombre = rdr["nombre"].ToString();
            ComentarioBlog.Apellidos = rdr["apellidos"].ToString();
            ComentarioBlog.Foto = rdr["foto"].ToString();
            ComentarioBlog.Comentario = rdr["comentario"].ToString();
            ComentarioBlog.Fecha = Convert.ToDateTime(rdr["fecha"]);
            ComentarioBlog.Like = Convert.ToInt32(rdr["like"]);
            ComentarioBlog.DisLike = Convert.ToInt32(rdr["dislike"]);
            
            return ComentarioBlog; 
        }
        public List<Object> ObtenerComentarios(int idBlog)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "SELECT cv.*, u.nombre, u.apellidos, u.foto";
                sql += " FROM comentarioblog cv";
                sql += " left join usuario u";
                sql += " on u.idUsuario=cv.idUsuario";
                sql += " WHERE cv.idBlog=" +idBlog;
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> resultado = new List<Object>();
              
                while (rdr.Read())
                {
                    ComentarioBlog cv = (ComentarioBlog)ToModel(rdr);
                    resultado.Add(cv);
                }
                rdr.Close();
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

    }

}