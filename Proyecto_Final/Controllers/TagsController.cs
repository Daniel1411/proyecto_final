﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class TagsController : BaseController
    {
        // GET: Tags
        public ActionResult Index()
        {
            return View();
        }
         public TagsController() : base()
        {
           this._model = new Tags();
           this._table = "tags";
        }


        public override Object ToModel(MySqlDataReader rdr)
        {
            Tags Tags = new Tags();
            Tags.IdTags = Convert.ToInt32(rdr["idTags"]);
            Tags.Tag = rdr["tag"].ToString();         
            return Tags; 
        }
    }
}