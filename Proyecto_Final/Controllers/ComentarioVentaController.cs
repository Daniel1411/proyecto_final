﻿using MySql.Data.MySqlClient;
using Proyecto_Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_Final.Controllers
{
    public class ComentarioVentaController : BaseController
    {
        public double Puntuacion { get; set; }

        public ComentarioVentaController() : base()
        {
            this._model = new ComentarioVenta();
            this._table = "comentarioventa";
            this.Puntuacion = 0;
        }

        public List<Object> ObtenerComentarios(int idUsuario)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "SELECT cv.*, u.nombre, u.apellidos, u.foto";
                sql += " FROM ventas v, comentarioventa cv";
                sql += " left join usuario u";
                sql += " on u.idUsuario=cv.idUsuario";
                sql += " WHERE (idVendedor='" + idUsuario + "' and v.idComprador is not null and v.idComentarioComprador=cv.idComentario) or (idComprador='" + idUsuario + "' and v.idVendedor is not null and v.idComentarioVendedor=cv.idComentario);";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Object> resultado = new List<Object>();
                int totalComentarios = 0;
                while (rdr.Read())
                {
                    ComentarioVenta cv = (ComentarioVenta) ToModel(rdr);
                    this.Puntuacion += cv.Puntuacion;
                    ++totalComentarios;
                    resultado.Add(cv);
                }
                if (totalComentarios == 0)
                {
                    this.Puntuacion = 0;
                }
                else
                {
                    this.Puntuacion = this.Puntuacion / totalComentarios;
                }
                rdr.Close();
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public override Object ToModel(MySqlDataReader rdr)
        {
            ComentarioVenta comentarioVenta = new ComentarioVenta();
            comentarioVenta.IdComentario = Convert.ToInt32(rdr["idComentario"]);
            comentarioVenta.IdUsuario = Convert.ToInt32(rdr["idUsuario"]);
            comentarioVenta.NombreUsuario = rdr["nombre"].ToString();
            comentarioVenta.ApellidosUsuario = rdr["apellidos"].ToString();
            comentarioVenta.FotoUsuario = rdr["foto"].ToString();
            comentarioVenta.Comentario = rdr["comentario"].ToString();
            comentarioVenta.Fecha = Convert.ToDateTime(rdr["fecha"]);
            comentarioVenta.Puntuacion = Convert.ToInt32(rdr["puntuacion"]);
            return comentarioVenta;
        }
    }
}