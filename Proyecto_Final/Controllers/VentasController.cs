﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class VentasController : BaseController
    {
        // GET: Ventas
        public ActionResult Index()
        {
            return View();
        }

         public VentasController() : base()
        {
           this._model = new Ventas();
           this._table = "ventas";
        }

        public void CrearVenta(int idComprador, int idVendedor, int idGallina, double valorVenta)
        {
            string connStr = "server=127.0.0.1;user=root;database=gallinas;port=3306;password=admin";
            MySqlConnection conn = new MySqlConnection(connStr);
            if (conn.State == System.Data.ConnectionState.Closed)
            {
                conn.Open();
            }


            try
            {
                string sql = "INSERT INTO ventas (idComprador, idVendedor,idGallina,valorVenta)";
                sql += " VALUES (@IdComprador, @IdVendedor,@IdGallina,@ValorVenta)";
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@IdComprador", idComprador);
                cmd.Parameters.AddWithValue("@IdVendedor", idVendedor);
                cmd.Parameters.AddWithValue("@IdGallina", idGallina);
                cmd.Parameters.AddWithValue("@ValorVenta", valorVenta);
                cmd.Prepare();
                cmd.ExecuteNonQuery();

                conn.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                conn.Close();
            }
        }

        public List<ComentarioVentaPendiente> ComprobarVentasUsuario(int loggedid)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "SELECT v.*, u.nombre nombreVendedor, g.nombre nombreGallina";
                sql += " FROM ventas v";
                sql += " left join usuario u on u.idUsuario=v.idVendedor";
                sql += " left join gallina g on g.idGallina=v.idGallina";
                sql += " WHERE v.idComprador=" + loggedid + " and v.idVendedor is not null and v.valorCompra is not null and v.idComentarioComprador is null;";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<ComentarioVentaPendiente> resultado = new List<ComentarioVentaPendiente>();
                while (rdr.Read())
                {
                    resultado.Add(ToComentarioVentaPendiente(rdr));
                }
                rdr.Close();
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
        }

        public ActionResult RegistrarComentarioComprador(int inputIdGallina, int inputIdVendedor, string textAreaComentario, int estrellas)
        {
            int loggedid = 0;
            if (!String.IsNullOrEmpty(Session["logged"] as String))
            {
                loggedid = Convert.ToInt32(Session["loggedid"]);
            }
            this.RegistrarComentario(loggedid, textAreaComentario, estrellas);
            int idComentarioVenta = this.ObtenerIdComentarioVenta();
            this.RegistrarComentarioCompradorEnVenta(inputIdGallina, inputIdVendedor, loggedid, idComentarioVenta);
            return RedirectToAction("Perfil", "Usuario", new { idUsuario = loggedid });
        }

        public ActionResult RegistrarVenta(int inputIdGallina, int selectUsuario, string inputPrecioFinal, string textAreaComentario, int estrellas)
        {
            int loggedid = 0;
            if (!String.IsNullOrEmpty(Session["logged"] as String))
            {
                loggedid = Convert.ToInt32(Session["loggedid"]);
            }
            this.RegistrarComentario(loggedid, textAreaComentario, estrellas);
            int idComentarioVenta = ObtenerIdComentarioVenta();
            this.RegistrarComentarioVendedorEnVenta(inputIdGallina, selectUsuario, inputPrecioFinal, loggedid, idComentarioVenta);
            this.MarcarGallinaVendida(inputIdGallina);
            return RedirectToAction("Perfil", "Usuario", new { idUsuario = loggedid });
        }

        private void MarcarGallinaVendida(int inputIdGallina)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "UPDATE gallina g";
                sql += " SET g.ventas=1";
                sql += " WHERE g.idGallina=" + inputIdGallina + ";";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        private void RegistrarComentario(int loggedid, string textAreaComentario, int estrellas)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "INSERT INTO comentarioventa(idUsuario, comentario, puntuacion)";
                sql += " VALUES(" + loggedid + ", '" + textAreaComentario + "', " + estrellas + ");";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        public int ObtenerIdComentarioVenta()
        {
            int id = 0;
            try
            {
                if (_conn.State == System.Data.ConnectionState.Closed)
                {
                    _conn.Open();
                }
                string sql = string.Format("select last_insert_id() as ultimo");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    id = Convert.ToInt32(rdr["ultimo"]);
                }

                _conn.Close();
                return id;
            }
            catch (Exception e)
            {
                Console.WriteLine();
                return id;
            }
            finally
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }

            }

        }

        private void RegistrarComentarioCompradorEnVenta(int idGallina, int idVendedor, int idComprador, int idComentarioCompra)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "UPDATE ventas v";
                sql += " SET v.idComentarioComprador=" + idComentarioCompra;
                sql += " WHERE v.idComprador=" + idComprador + " and v.idVendedor=" + idVendedor + " and v.idGallina=" + idGallina + ";";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }
        
        private void RegistrarComentarioVendedorEnVenta(int idGallina, int selectUsuario, string inputPrecioFinal, int idComprador, int idComentarioVenta)
        {
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "UPDATE ventas v";
                sql += " SET v.valorCompra=" + inputPrecioFinal + ", v.idComentarioVendedor=" + idComentarioVenta;
                sql += " WHERE v.idComprador=" + selectUsuario + " and v.idVendedor=" + idComprador + " and v.idGallina=" + idGallina + ";";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                rdr.Close();
                _conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

        public ActionResult FormularioVendedor(int idUsuario, string razaGallina, int idGallina)
        {
            FormularioVenta formularioVenta = new FormularioVenta();
            formularioVenta.Compradores = this.ObtenerCompradores(idUsuario);
            formularioVenta.NombreGallina = razaGallina;
            formularioVenta.IdGallina = idGallina;
            return View(formularioVenta);
        }

        public ActionResult FormularioComprador(int idVendedor, string nombreVendedor, string razaGallina, int idGallina)
        {
            FormularioVenta formularioVenta = new FormularioVenta();
            formularioVenta.NombreUsuario = nombreVendedor;
            formularioVenta.NombreGallina = razaGallina;
            formularioVenta.IdGallina = idGallina;
            formularioVenta.IdVendedor = idVendedor;
            return View(formularioVenta);
        }

        private List<Tuple<string, string>> ObtenerCompradores(int idUsuario)
        {
            
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "SELECT v.idComprador, u.nombre nombreComprador, u.apellidos apellidosComprador";
                sql += " FROM ventas v";
                sql += " left join usuario u on v.idComprador=u.idUsuario";
                sql += " where v.idVendedor='" + idUsuario + "';";
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();
                List<Tuple<string, string>> resultado = new List<Tuple<string, string>>();
                while (rdr.Read())
                {
                    resultado.Add(ToComprador(rdr));
                }
                _conn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;
            }
            throw new NotImplementedException();
        }

        private Tuple<string, string> ToComprador(MySqlDataReader rdr)
        {
            string nombre = rdr["nombreComprador"].ToString();
            string apellidos = rdr["apellidosComprador"].ToString();
            int idComprador = Convert.ToInt32(rdr["idComprador"]);
            Tuple<string, string> comprador = new Tuple<string, string>(nombre + " " + apellidos, idComprador.ToString());
            return comprador;
        }

        public ComentarioVentaPendiente ToComentarioVentaPendiente(MySqlDataReader rdr)
        {
            ComentarioVentaPendiente Ventas = new ComentarioVentaPendiente();
            Ventas.IdVentas = Convert.ToInt32(rdr["idVentas"]);
            Ventas.IdComprador = Convert.ToInt32(rdr["idComprador"]);
            Ventas.ValorCompra = Convert.ToDouble(rdr["valorCompra"]);
            Ventas.ValorVenta = Convert.ToDouble(rdr["valorVenta"]);
            Ventas.IdGallina = Convert.ToInt32(rdr["idGallina"]);
            Ventas.IdVendedor = Convert.ToInt32(rdr["idVendedor"]);
            Ventas.NombreGallina = rdr["nombreGallina"].ToString();
            Ventas.NombreVendedor = rdr["nombreVendedor"].ToString();
            return Ventas;
        }

        public override Object ToModel(MySqlDataReader rdr)
        {
            Ventas Ventas = new Ventas();
            Ventas.IdVentas = Convert.ToInt32(rdr["idVentas"]);
            Ventas.IdComprador = Convert.ToInt32(rdr["idComprador"]);
            Ventas.ValorCompra = Convert.ToDouble(rdr["valorCompra"]);
            Ventas.ValorVenta = Convert.ToDouble(rdr["valorVenta"]);
            Ventas.IdGallina = Convert.ToInt32(rdr["idGallina"]);
            Ventas.IdVendedor = Convert.ToInt32(rdr["idVendedor"]);
            Ventas.IdComentarioComprador = Convert.ToInt32(rdr["idComentarioComprador"]);
            Ventas.IdComentarioVendedor = Convert.ToInt32(rdr["idComentarioVendedor"]);
            return Ventas; 
        }
        public ActionResult CreateVenta(int idUsuarioReceptor)
        {
            this.CrearVentaEnBD(idUsuarioReceptor);
            //obtener el ultimo id
            int idChat = this.ObtenerIdChat();
            return RedirectToAction("Chat", new { idchat = idChat });
        }

        private int ObtenerIdChat()
        {
            int IdChat = -1;
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = string.Format("select last_insert_id() as ultimo");
                MySqlCommand cmd = new MySqlCommand(sql, _conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    IdChat = Convert.ToInt32(rdr["ultimo"]);
                }

                _conn.Close();
                return IdChat;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return -1;
            }
        }

        private void CrearVentaEnBD(int idUsuarioReceptor)
        {
            int loggedid = 0;
            if (!String.IsNullOrEmpty(Session["logged"] as string))
            {
                loggedid = Convert.ToInt32(Session["loggedid"]);
            }
            int IdComprador = loggedid;
            int IdVendedor = -1;
            
            if (_conn.State == System.Data.ConnectionState.Closed)
            {
                _conn.Open();
            }
            try
            {
                string sql = "INSERT INTO ventas (idComprador, idVendedor,idGallina,valorCompra,valorVenta,idComentarioComprador,idComentarioVendedor)";
                sql += " VALUES (@IdComprador, @IdVendedor,@IdGallina,@ValorCompra,@ValorVenta,@IdComentarioComprador,@IdComentarioVendedor)";
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = sql;
                cmd.Connection = _conn;
                cmd.Parameters.AddWithValue("@IdComprador", IdComprador);
                cmd.Parameters.AddWithValue("@IdUsuarioReceptor", IdVendedor);
                /*
                cmd.Parameters.AddWithValue("@IdUsuarioReceptor", IdGallina);
                cmd.Parameters.AddWithValue("@IdUsuarioReceptor", ValorCompra);
                cmd.Parameters.AddWithValue("@IdUsuarioReceptor", ValorVenta);
                cmd.Parameters.AddWithValue("@IdUsuarioReceptor", IdComentarioComprador);
                cmd.Parameters.AddWithValue("@IdUsuarioReceptor", IdComentarioVendedor);*/
                cmd.Prepare();
                cmd.ExecuteNonQuery();

                _conn.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
            }
        }

    }
}