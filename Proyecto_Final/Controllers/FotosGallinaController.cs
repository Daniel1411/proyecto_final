﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class FotosGallinaController : BaseController
    {
        // GET: FotosGallina
        public ActionResult Index()
        {
            return View();
        }
         public FotosGallinaController() : base()
        {
           this._model = new FotosGallina();
           this._table = "fotosGallina";
        }


        public override Object ToModel(MySqlDataReader rdr)
        {
            FotosGallina FotosGallina = new FotosGallina();
            FotosGallina.IdFotosGallina = Convert.ToInt32(rdr["idFotosGallina"]);          
            FotosGallina.Foto1 = rdr["foto1"].ToString();
            FotosGallina.Foto2 = rdr["foto2"].ToString();           
            FotosGallina.Foto3 = rdr["foto3"].ToString();          
            FotosGallina.IdGallina = Convert.ToInt32(rdr["idGallina"]);
            return FotosGallina; 
        }
    }
}