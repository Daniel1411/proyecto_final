﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class RazaController : BaseController
    {
        // GET: Raza
        public ActionResult Index()
        {
            return View();
        }

         public RazaController() : base()
        {
           this._model = new Raza();
           this._table = "raza";
        }

        public override Object ToModel(MySqlDataReader rdr)
        {
            Raza Raza = new Raza();
            Raza.IdRaza = Convert.ToInt32(rdr["idRaza"]);         
            Raza.Nombre = rdr["raza"].ToString();           
            return Raza; 
        }

         public List<Raza> GetAll()
        {
            MySqlConnection _conn = null;
            string connStr = "server=localhost;user=root;database=gallinas;port=3306;password=admin";

            List<Raza> lista = new List<Raza>();//creamos una lista para recoger tofdos los datos

            try//bloque try para controlar las excepciones.
            {
                _conn = new MySqlConnection(connStr);//utilizamos el constructor para crear el objeto de conexion

                string tabla = "raza";
                _conn.Open();//abrimos la sesion con el servidor SQl
                string sql = string.Format("SELECT * FROM {0}  order by raza asc",tabla);//metemos la consulta SQL en un string 
                MySqlCommand cmd = new MySqlCommand(sql, _conn);//le pasamos la consulta y el obejo que habre la conexion
                MySqlDataReader rdr = cmd.ExecuteReader();//este objeto nos devuelve los datos que hemos consultado 

                while (rdr.Read())//se situa en la primera posicion si hay datos entra en el if sino hay datos devovera falso
                {
                    Raza al = new Raza();//rellenamos el objeto con los datos que nos devuelve la consulta 
                    al.IdRaza = (int)rdr["idRaza"];
                    al.Nombre = rdr["raza"].ToString();//dentro de los corchetes escribimos exactamente el nombre de los campos de la base de datos
                    

                    lista.Add(al);//cada vez que da una vuelta al bucle añade un objeto alumno

                }
                rdr.Close();//cerramos el evento rdr

            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.ToString());
                _conn.Close();
                return null;

            }
            finally//siempre llegamos aqui ocurra lo que ocurra
            {
                if (_conn != null && _conn.State == System.Data.ConnectionState.Open)//se asegura de que la conexion a la base de datos se queda cerrada
                {
                    _conn.Close();
                }
            }
            return lista;//devuelve un objeto con los datos de la base de datos

        }
    }
}