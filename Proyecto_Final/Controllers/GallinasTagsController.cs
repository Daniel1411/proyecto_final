﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Final.Models;
using MySql.Data.MySqlClient;

namespace Proyecto_Final.Controllers
{
    public class GallinasTagsController : BaseController
    {
        // GET: GallinasTags
        public ActionResult Index()
        {
            return View();
        }
         public GallinasTagsController() : base()
        {
           this._model = new GallinasTags();
           this._table = "gallinahastags";
        }


        public override Object ToModel(MySqlDataReader rdr)
        {
            GallinasTags GallinasTags = new GallinasTags();
            GallinasTags.IdGallinas = Convert.ToInt32(rdr["idGallina"]);
            GallinasTags.IdTgas = Convert.ToInt32(rdr["idTags"]);
           
            return GallinasTags; 
        }
    }
}