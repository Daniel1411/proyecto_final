﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Raza
    {
        public int IdRaza { get; set; }
        public string Nombre { get; set; }
    }
}