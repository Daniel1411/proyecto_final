﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class ComentarioBlog
    {
        public int IdComentarioBlog { get; set; }
        public int IdBlog { get; set; }
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Foto { get; set; }
        public string Comentario { get; set; }
        public DateTime Fecha { get; set; }
        public int Like { get; set; }
        public int DisLike { get; set; }
    }
}