﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Mensaje
    {
        public int IdMensaje { get; set; }
        public int IdChat { get; set; }
        public string Texto { get; set; }
        public string Fecha { get; set; }
        public int IdAutor { get; set; }
    }
}