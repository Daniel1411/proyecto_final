﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Provincia
    {
        public int IdProvincias { get; set; }
        public string Nombre { get; set; }
    }
}