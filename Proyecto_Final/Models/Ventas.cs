﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Ventas
    {
        public int IdVentas { get; set; }
        public int IdComprador{ get; set; }
        public int IdVendedor{ get; set; }
        public int IdGallina { get; set; }
        public double ValorCompra{ get; set; }
        public double ValorVenta{ get; set; }
        public List<string> Compradores { get; set; }
        public int IdComentarioComprador { get; set; }
        public int IdComentarioVendedor { get; set; }
    }
}