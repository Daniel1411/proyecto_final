﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Infoblog
    {
        public Blog Blog { get; set; }
        public List<ComentarioBlog> Comentarios { get; set; }
    }
}