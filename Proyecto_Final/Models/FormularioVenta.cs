﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class FormularioVenta
    {
        public List<Tuple<string, string>> Compradores { get; set; }
        public string NombreGallina { get; set; }
        public string NombreUsuario { get; set; }
        public int IdGallina { get; set; }
        public int IdVendedor { get; set; }
    }
}