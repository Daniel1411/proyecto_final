﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Favorito
    {
        public int IdFavorito { get; set; }
        public int IdGallina { get; set; }
        public int IdUsuario { get; set; }
    }
}