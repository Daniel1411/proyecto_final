﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_Final.Models
{
    public class Usuario
    {
        public int IdUsuario { get; set; } // no necesaria
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Apellidos { get; set; }
        [Required]
        public DateTime Edad { get; set; }
        
        public DateTime FechadeAlta { get; set; } // no necesaria
        [Required]
        public int IdProvincias { get; set; }
        public string Provincia { get; set; }
        public double Puntuacion { get; set; }
        public List<ComentarioVenta> Comentarios { get; set; }
        public List<Gallina> Gallinas { get; set; }
        public List<ComentarioVentaPendiente> ComentariosPendientesVentas { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "{0} ha de tenir entre {2} i {1} caracters.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        [Display(Name = "Confirmació de password")]
        public string ConfirmPassword { get; set; }


        public string Foto { get; set; }
    }

}