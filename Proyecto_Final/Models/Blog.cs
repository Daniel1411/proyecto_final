﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Blog
    {
        public int IdBlog { get; set; }
        public int IdUsuario { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public string Resumen { get; set; }
        public DateTime Fecha { get; set; }
        public string Foto { get; set; }
        public int Like { get; set; }
        public int DisLike { get; set; }
        public int Views { get; set; }
    }
}