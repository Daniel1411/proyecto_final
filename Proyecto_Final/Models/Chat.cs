﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Chat
    {
        public int IdChat { get; set; }
        public int IdUsuarioEmisor { get; set; }
        public int IdUsuarioReceptor { get; set; }
        public string NombreEmisor { get; set; }
        public string NombreReceptor { get; set; }
        public string FotoEmisor { get; set; }
        public string FotoReceptor { get; set; }
    }
}