﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class ComentarioVentaPendiente
    {
        public int IdVentas { get; set; }
        public int IdComprador { get; set; }
        public int IdVendedor { get; set; }
        public string NombreVendedor { get; set; }
        public int IdGallina { get; set; }
        public string NombreGallina { get; set; }
        public double ValorCompra { get; set; }
        public double ValorVenta { get; set; }
    }
}