﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Gallina
    {
        public int IdGallina { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public int IdRaza { get; set; }
        public string Raza { get; set; }
        public int IdUsuario { get; set; }
        public DateTime Fecha_Alta { get; set; }
        public string Fecha { get; set; }
        public string Dimensiones { get; set; }
        [Required]
        public string Sexo { get; set; }
        public string Descripcion { get; set; }
        [Required]
        public string Localidad { get; set; }
        public string[] Tags { get; set; }
        [Required]
        public double Precio { get; set; }
        public double Peso { get; set; }
        public int ValorHuevos { get; set; }
        public string Audio { get; set; }
        public int Views { get; set; }
        public bool Venta { get; set; }
        public string Foto { get; set; }
        public string[] Fotos { get; set; }
        public int HuevosGallina { get; set; }
        public string AudioGallo { get; set; }
        public bool Ventas { get; set; }

        public string FotoUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Favorito { get; set; }

       

        public string NomUsuario { get; set; }
        public string FotUsuario { get; set; }

    }
}