﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class Home
    {
        public List<Gallina> Gallinas { get; set; }
        public List<Raza> Razas { get; set; }
        public List<Provincia> Provincias { get; set; }
        public List<Blog> Blogs { get; set; } 
    }
}