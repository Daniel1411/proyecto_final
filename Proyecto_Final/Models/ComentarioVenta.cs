﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_Final.Models
{
    public class ComentarioVenta
    {
        public int IdComentario { get; set; }
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string ApellidosUsuario { get; set; }
        public string FotoUsuario { get; set; }
        public string Comentario { get; set; }
        public DateTime Fecha { get; set; }
        public int Puntuacion { get; set; }
    }
}